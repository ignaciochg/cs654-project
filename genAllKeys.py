#!/usr/bin/env python3

from Crypto.PublicKey import RSA

# https://pycryptodome.readthedocs.io/en/latest/src/public_key/rsa.html
key = RSA.generate(2048)
private_key = key.export_key()
file_out = open("private.pem", "wb")
file_out.write(private_key)

public_key = key.publickey().export_key()
file_out = open("receiver.pem", "wb")
file_out.write(public_key)


# gold 
# https://pycryptodome.readthedocs.io/en/latest/src/examples.html
# https://pycryptodome.readthedocs.io/en/latest/src/hash/sha3_512.html