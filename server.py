#!/usr/bin/env python3

# imporing required modules 
import socket
import sys
import time

from my_crypto import * 

# program variables
ENC_MODE = SHA3_ENC



if ENC_MODE == NO_ENC:
   enc_type = "NO_ENC"
elif ENC_MODE == DES_ENC:
   enc_type = "DES_ENC"
elif ENC_MODE == DES3_ENC:
   enc_type = "DES3_ENC"
elif ENC_MODE == AES_ENC:
   enc_type = "AES_ENC"
elif ENC_MODE == RSA_ENC:
   enc_type = "RSA_ENC"
elif ENC_MODE == SHA3_ENC:
   enc_type = "SHA3_ENC"
print("Using {}.\n".format(enc_type))


# initializing socket 
s = socket.socket()      
host = "0.0.0.0"#socket.gethostname()   
port = 2001
exportKeys("server.ring")

s.bind((host, port))    
  
s.listen(5)   
c, addr = s.accept()    
importOtherKeyRing("client.ring")

try:

   while True: 
      recieved = c.recv(10000)
      recieved_time = time.time()
      if len(recieved) == 0:
         c.close()
         sys.exit(0)
      decoded = dec(ENC_MODE,recieved)
      valid = decoded[0]
      plain = decoded[1].strip()

      if not valid:
         print ("[{}] rcved-invalid: {}".format(recieved_time,plain))
      else:
         print ("[{}] rcved: {}".format(recieved_time,plain))

      cypher = enc(ENC_MODE,"Nice to see you")
      print("sending:",cypher)
      c.send(cypher)
except BrokenPipeError as e:
   c.close() 