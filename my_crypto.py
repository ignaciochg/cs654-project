#!/ur/bin/env python3 

from Crypto.Cipher import DES
from Crypto.Cipher import DES3
from Crypto.Cipher import AES
from Crypto.PublicKey import RSA
from Crypto.Cipher import  PKCS1_OAEP # RSA standard
from Crypto.Hash import SHA3_512
from Crypto.Random import get_random_bytes
import pickle


NO_ENC = 0
DES_ENC = 1
DES3_ENC = 2
AES_ENC = 3
RSA_ENC = 4
SHA3_ENC = 5


# generate keys

DES_key = get_random_bytes(8)
DES3_key = get_random_bytes(16)
AES_key = get_random_bytes(16)
RSA_key = RSA.generate(2048)
RSA_pu_key = RSA_key.publickey()
RSA_pr_key = RSA_key

# defining keys for other party

DES_key_other = None
DES3_key_other = None
AES_key_other = None
RSA_pu_key_other = None


def timeToSTR(elapsedTime):
   hours = elapsedTime//3600
   temp = elapsedTime - 3600*hours
   minutes = temp//60
   temp = temp - 60*minutes
   seconds = int(temp)
   millis = (temp - seconds) * 1000
   return '%dh:%dm:%ds:%dms' %(hours,minutes,seconds,millis)

def sha3(message):
   hasher = SHA3_512.new()
   hasher.update(message.encode())
   return hasher

def RSA_encrypt(msg, key):
   cipher_rsa = PKCS1_OAEP.new(key)
   return cipher_rsa.encrypt(msg.encode())
def RSA_decrypt(cypher,key):
   cipher_rsa = PKCS1_OAEP.new(key)
   msg = cipher_rsa.decrypt(cypher)
   return msg.decode()

def AES_encrypt(msg, key):
   cipher_aes = AES.new(key, AES.MODE_CBC)
   size = len(msg)%16
   if size != 0:
      padding = " "*(16-size)
   cipher = cipher_aes.iv + cipher_aes.encrypt(msg.encode()+padding.encode())
   return cipher
def AES_decrypt(chipher, key):
   cipher_aes = AES.new(key, AES.MODE_CBC)
   msg = cipher_aes.decrypt(chipher)[len(cipher_aes.iv):] 
   return msg.decode().strip()


def DES_encrypt(msg, key):
   cipher_des = DES.new(key, DES.MODE_CBC)
   size = len(msg)%8
   if size != 0:
      padding = " "*(8-size)
   cipher = cipher_des.iv + cipher_des.encrypt(msg.encode()+padding.encode())
   return cipher
def DES_decrypt(chipher, key):
   cipher_des = DES.new(key, DES.MODE_CBC)
   msg = cipher_des.decrypt(chipher)[len(cipher_des.iv):] 
   return msg.decode().strip()


def DES3_encrypt(msg, key):
   cipher_des = DES3.new(key, DES3.MODE_CBC)
   size = len(msg)%8
   if size != 0:
      padding = " "*(8-size)
   cipher = cipher_des.iv + cipher_des.encrypt(msg.encode()+padding.encode())
   return cipher
def DES3_decrypt(chipher, key):
   cipher_des = DES3.new(key, DES3.MODE_CBC)
   msg = cipher_des.decrypt(chipher)[len(cipher_des.iv):] 
   return msg.decode().strip()

def enc(method, message):
   if method == NO_ENC:
      return message.encode()
   elif method == DES_ENC:
      return DES_encrypt(message,DES_key)
   elif method == DES3_ENC:
      return DES3_encrypt(message,DES3_key)
   elif method == AES_ENC:
      # print("key:",AES_key)
      return AES_encrypt(message,AES_key)
   elif method == RSA_ENC:
      return RSA_encrypt(message,RSA_pu_key_other)
   elif method == SHA3_ENC:
      content = (message,sha3(message).digest())
      output = pickle.dumps(content)
      return output
   return -1

def dec(method, cypher):
   if method == NO_ENC:
      return True, cypher#.decode()
   elif method == DES_ENC:
      return True,DES_decrypt(cypher,DES_key_other)
   elif method == DES3_ENC:
      return True,DES3_decrypt(cypher,DES3_key_other)
   elif method == AES_ENC:
      # print("key:",AES_key_other)
      return True,AES_decrypt(cypher,AES_key_other)
   elif method == RSA_ENC:
      return True,RSA_decrypt(cypher,RSA_pr_key)
   elif method == SHA3_ENC:
      recieved = pickle.loads(cypher)
      msg = recieved[0]
      # print("msg",msg)
      if sha3(msg).digest() == recieved[1]:
         valid = True
      else:
         valid = False
      return valid,msg
   return -1




def exportKeys(location):
   global DES_key,DES3_key,AES_key,RSA_pu_key

   keyRing = (DES_key,DES3_key,AES_key,RSA_pu_key.export_key())

   pickle.dump(keyRing,open(location,"wb"))


def importOtherKeyRing(location):
   global DES_key_other,DES3_key_other,AES_key_other,RSA_pu_key_other

   other_keyring = pickle.load( open( location, "rb" ) )

   DES_key_other = other_keyring[0]
   DES3_key_other = other_keyring[1]
   AES_key_other = other_keyring[2]
   RSA_pu_key_other = RSA.import_key(other_keyring[3])