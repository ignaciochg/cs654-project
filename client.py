#!/usr/bin/env python3

import socket
import threading
import time
import csv
import os

from my_crypto import *

# program variables
ENC_MODE = SHA3_ENC

host = "127.0.0.1"
port = 2001
run_for_ins_s = 60*5 # seconds
delay = 2 # seconds



if ENC_MODE == NO_ENC:
   enc_type = "NO_ENC"
elif ENC_MODE == DES_ENC:
   enc_type = "DES_ENC"
elif ENC_MODE == DES3_ENC:
   enc_type = "DES3_ENC"
elif ENC_MODE == AES_ENC:
   enc_type = "AES_ENC"
elif ENC_MODE == RSA_ENC:
   enc_type = "RSA_ENC"
elif ENC_MODE == SHA3_ENC:
   enc_type = "SHA3_ENC"
print("Using {}.\n".format(enc_type))


def quit(): 
   global exit
   exit = True 
  

exit = False
s = socket.socket() 
exportKeys("client.ring")

data = list()
data.append(["Start_Time","End_Time","Total_RTT", "Sent_Time", "Recieved_Time","Net_only_RTT"])

timer = threading.Timer(run_for_ins_s, quit) 
timer.start()
s.connect((host, port))
time.sleep(1) # sleep so the server has time to generate/export and import keys 
importOtherKeyRing("server.ring")

while not exit:
   start_t = time.time()
   c = enc(ENC_MODE,"hello")
   print("sending:",c)

   sent_time = time.time()
   s.send(c)
   recieved = s.recv(10000)
   recieved_time = time.time()

   decoded = dec(ENC_MODE,recieved)
   valid = decoded[0]
   plain = decoded[1].strip()
   end_t = time.time()

   if not valid:
      print ("[{}] rcved-invalid: {}".format(end_t,plain))
   else:
      print ("[{}] rcved: {}".format(end_t,plain))

   data.append([start_t,end_t,end_t-start_t, sent_time, recieved_time, recieved_time-sent_time ])
   # print("RTT:",timeToSTR(end_t-start_t))
   time.sleep(delay)


print("Done communicating\n")
s.close()

# prep data & print it 

for dat in data:
   print(dat)

if ENC_MODE == NO_ENC:
   output_file = "NO_ENC"
elif ENC_MODE == DES_ENC:
   output_file = "DES_ENC"
elif ENC_MODE == DES3_ENC:
   output_file = "DES3_ENC"
elif ENC_MODE == AES_ENC:
   output_file = "AES_ENC"
elif ENC_MODE == RSA_ENC:
   output_file = "RSA_ENC"
elif ENC_MODE == SHA3_ENC:
   output_file = "SHA3_ENC"

output_file_full = "output/{}-{}s.csv".format(output_file,run_for_ins_s)
print("Exporting to {}".format(output_file_full))

try:
   os.mkdir("output")
except:
   pass
with open(output_file_full, 'w') as writeFile:
   writer = csv.writer(writeFile)
   writer.writerows(data)